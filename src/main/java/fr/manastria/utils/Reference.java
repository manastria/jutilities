package fr.manastria.utils;

import java.text.MessageFormat;

/**
 * Created by Jean-Philippe on 28/10/2015.
 */
public class Reference {
	/**
	 * Affiche la référence d'un objet sous la forme type@reference
	 * @param o : l'objet dont on veut la référence
	 * @return la référence de l'objet sous la forme type@reference
	 */
	public static String toIdentityString(Object o) {
		if (o == null) {
			return "null";
		}
		return o.getClass().getName() + "@"
				+ Integer.toHexString(System.identityHashCode(o));
	}

	/**
	 * Affiche le type, la référence d'un objet ainsi que son contenu sous forme de chaine de caractère.
	 * @param variableName : le nom de la variable à afficher
	 * @param obj : l'objet a afficher
	 * @return Une chaine de caractère sous la forme &lt;nom variable&gt; (&lt;type&gt;@&lt;reference&gt;) : &lt;contenu&gt;
	 */
	public static String info(String variableName, Object obj) {
		return MessageFormat.format("{2} ({1}) : {0}", obj.toString(), toIdentityString(obj), variableName);
	}
}
