package fr.manastria.utils;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Jean-Philippe
 *
 */
public class Console {
	
	private static final Logger logger = LoggerFactory.getLogger(Console.class);
	/**
	 * Afficher un prompt à l'écran (sans passage à la ligne)
	 * 
	 * @param prompt
	 *          texte à afficher à l'écran
	 */
	public static void printPrompt(String prompt) {
		System.out.print(prompt + " ");
		System.out.flush(); // forcer l'écriture en l'absence de '\n'
	}

	/**
	 * Affiche un message à l'écran suivi d'un saut de ligne
	 * 
	 * @param format Le message
	 * @param arguments  Les éléments à afficher
	 *          texte à afficher à l'écran
	 * 
	 */
	public static void println(String format, Object... arguments) {
		FormattingTuple tp = MessageFormatter.arrayFormat(format, arguments);
		String prompt = tp.getMessage();

		System.out.println(prompt);
	}

	/**
	 * Lire une chaîne de caractères à partir du clavier. La chaîne de caractères
	 * est terminée par un retour à la ligne (qui ne fait pas partie du résultat).
	 *
	 * @return la ligne lue à partir du clavier (sans le retour à la ligne)
	 */
	public static String readLine() {
		int characterRead;
		String r = "";
		boolean done = false;
		while (!done) {
			try {
				characterRead = System.in.read();
				if (characterRead < 0 || (char) characterRead == '\n') {
					done = true;
				} else if ((char) characterRead != '\r') {
					r = r + (char) characterRead;
				}
			} catch (java.io.IOException e) {
				done = true;
			}
		}
		return r;
	}

	/**
	 * Même méthode que readLine avec en plus l'affichage d'un prompt.
	 * 
	 * @param prompt
	 *          texte à afficher à l'écran
	 * @return la ligne lue à partir du clavier (sans le retour à la ligne)
	 */
	public static String readLine(String prompt) {
		printPrompt(prompt);
		return readLine();
	}

	/**
	 * Lire un réel à partir du clavier (équivalent du readln de Pascal). Resaisie
	 * dans le cas d'une erreur de l'utilisateur.
	 * 
	 * @param prompt
	 *          texte à afficher à l'écran
	 * @return l'entier lu, 0 si l'entier n'a pu être convertie.
	 */
	public static double readDouble(String prompt) {
		double resultat = 0;
		boolean saisieOK = false;
		while (!saisieOK) {
			printPrompt(prompt);
			try {
				String s = readLine().trim();
				resultat = Double.parseDouble(s);
				saisieOK = true;
			} catch (NumberFormatException e) {
				System.out.println("Ce n'est pas un réel. Recommencez!");
			}
		}
		return resultat;
	}

	/**
	 * Convertir une chaine de caractère au format dd/MM/yyyy en date
	 * 
	 * @param strDate La chaine de caractère à convertir
	 * @return <p>La date si la chaine a pu être convertie. </p> 
	 * <p>Null sinon</p>
	 */
	public static Date parseDate(String strDate) {
		Date date = null;
		Locale localeFr = Locale.FRENCH;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			date = sdf.parse(strDate);
		} catch (ParseException ex) {
			logger.info("{}", ex);
		}

		return date;
	}

	/**
	 * Lire un entier à partir du clavier (équivalent du readln de Pascal).
	 * Resaisie dans le cas d'une erreur de l'utilisateur.
	 * 
	 * @param format
	 *          texte à afficher à l'écran
	 * @param arguments Liste
	 * @return l'entier lu
	 */
	public static Integer readInteger(String format, Object... arguments) {
		/* Définition du prompt */
		FormattingTuple tp = MessageFormatter.arrayFormat(format, arguments);
		String prompt = tp.getMessage();
		TryParseResult tpResult = null;

		do {
			printPrompt(prompt);
			String s = readLine().trim();
			tpResult = TryParse.tryParseInteger(s);
			if (!tpResult.parsed) {
				System.out.println("Ce n'est pas un entier. Recommencez!");
			}
		} while (!tpResult.parsed);

		return tpResult.getIntegerValue();
	}
}
