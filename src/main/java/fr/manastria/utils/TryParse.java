/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.manastria.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Jean-Philippe
 */
public class TryParse {
	
	private static final Logger logger = LoggerFactory.getLogger(TryParse.class);

    public static TryParseResult tryParseInteger(String value, Integer defaultValue) {
		TryParseResult result = tryParseInteger(value);

        if (! result.isParsed()) {
            result.integerValue = defaultValue;
            return result;
        }

        return result;
    }
	
	public static TryParseResult tryParseInteger(String value) {


        if (value == null) {
            return new TryParseResult(false, (Integer)null);
        }

        try {
            int r = Integer.parseInt(value.trim());
            return new TryParseResult(true, r);
        } catch (NumberFormatException e) {
            return new TryParseResult(false, (Integer)null);
        }
    }


    public static TryParseResult tryParseDouble(String value) {

        if (value == null) {
            return new TryParseResult(false, (Double)null);
        }

        try {
            double result = Double.parseDouble(value.trim());
            return new TryParseResult(true, result);
        } catch (NumberFormatException e) {
            return new TryParseResult(false, (Double)null);
        }
    }
}
