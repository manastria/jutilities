package fr.manastria.utils;

/**
 * Created by Jean-Philippe on 12/11/2015.
 */
public class TryParseResult {
	boolean parsed;
	Integer integerValue;
	Double  doubleValue;

	public TryParseResult(boolean passed, Integer value) {
		this.parsed = passed;
		this.integerValue = value;
	}


	public TryParseResult(boolean passed, Double value) {
		this.parsed = passed;
		this.doubleValue = value;
	}

	public TryParseResult(boolean passed, Float value) {
		this.parsed = passed;
		this.doubleValue = new Double(value);
	}

	public Integer getIntegerValue() {
		return integerValue;
	}

	public boolean isParsed() {
		return parsed;
	}

	public Double getDoubleValue() {
		return doubleValue;
	}
}
