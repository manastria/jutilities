package fr.manastria.logback;

import ch.qos.logback.core.rolling.TriggeringPolicyBase;

import java.io.File;

// Source : http://stackoverflow.com/questions/2492022/how-to-roll-the-log-file-on-startup-in-logback/2647471#2647471

public class RollOncePerSessionTriggeringPolicy<E> extends TriggeringPolicyBase<E> {
	private static boolean doRolling = true;

	public boolean isTriggeringEvent(File activeFile, E event) {
		// roll the first time when the event gets called
		if (doRolling) {
			doRolling = false;
			return true;
		}
		return false;
	}
}