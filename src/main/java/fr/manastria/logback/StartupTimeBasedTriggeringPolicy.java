package fr.manastria.logback;

import ch.qos.logback.core.joran.spi.NoAutoStart;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.RolloverFailure;

// Source : http://stackoverflow.com/questions/2492022/how-to-roll-the-log-file-on-startup-in-logback

/**
 * Allows rotation of the log files for each run
 * 
 * 
 * @author jpdemory
 *
 * @param <E> : TODO
 */
@NoAutoStart
public class StartupTimeBasedTriggeringPolicy<E> extends
		DefaultTimeBasedFileNamingAndTriggeringPolicy<E> {

	/**
	 * TODO
	 */
  @Override
  public void start() {
      super.start();
      nextCheck = 0L;
      isTriggeringEvent(null, null);
      try {
          tbrp.rollover();
      } catch (RolloverFailure e) {
          //Do nothing
      }
  }
}
