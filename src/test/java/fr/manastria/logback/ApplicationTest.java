package fr.manastria.logback;

import fr.manastria.utils.TryParse;
import fr.manastria.utils.TryParseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class ApplicationTest {

	private static final Logger logger = LoggerFactory
			.getLogger(ApplicationTest.class);

	@Test
	public void entier() {
		TryParseResult result;

		result = TryParse.tryParseInteger(null);
		Assert.assertEquals(result.isParsed(), false);


		// OK
		result = TryParse.tryParseInteger("160519");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getIntegerValue(), new Integer(160519));


		// Failed
		result = TryParse.tryParseInteger("9432.0");
		Assert.assertEquals(result.isParsed(), false);

		// Failed
		result = TryParse.tryParseInteger("16,667");
		Assert.assertEquals(result.isParsed(), false);

		// OK
		result = TryParse.tryParseInteger("   -322   ");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getIntegerValue(), new Integer(-322));

		// OK
		result = TryParse.tryParseInteger("+4302");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getIntegerValue(), new Integer(4302));

		//Failed
		result = TryParse.tryParseInteger("(100);");
		Assert.assertEquals(result.isParsed(), false);

		// Failed
		result = TryParse.tryParseInteger("01FA");
		Assert.assertEquals(result.isParsed(), false);
	}



	@Test
	public void reel() {
		TryParseResult result;

		result = TryParse.tryParseDouble(null);
		Assert.assertEquals(result.isParsed(), false);


		// OK
		result = TryParse.tryParseDouble("160519");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getDoubleValue(), 160519d);


		// OK
		result = TryParse.tryParseDouble("9432.0");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getDoubleValue(), new Double(9432.0));

		// Failed
		result = TryParse.tryParseDouble("16,667");
		Assert.assertEquals(result.isParsed(), false);

		// OK
		result = TryParse.tryParseDouble("   -322   ");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getDoubleValue(), (double) -322);

		// OK
		result = TryParse.tryParseDouble("+4302");
		Assert.assertEquals(result.isParsed(), true);
		Assert.assertEquals(result.getDoubleValue(), 4302d);

		//Failed
		result = TryParse.tryParseDouble("(100);");
		Assert.assertEquals(result.isParsed(), false);

		// Failed
		result = TryParse.tryParseDouble("01FA");
		Assert.assertEquals(result.isParsed(), false);
	}
}
